package co.kr.naver.unmannedstoremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductType {

    FROZEN("냉동식품"),
    COLD("냉장식품"),
    FRESH("신선식품"),
    SNACK("과자"),
    RAMEN("라면"),
    DRINK("음료"),
    STATIONERY("문구"),
    TOY("완구"),
    DISPOSABLE("일회용품"),
    IT("전자기기"),
    ETC("기타");

    private final String name;
}
