package co.kr.naver.unmannedstoremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InputStatus {

    COMPLETE("완료"),
    WAIT("예정"),
    CANCEL("취소");

    private final String name;

}
