package co.kr.naver.unmannedstoremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OutStatus {

    DELETE("폐기"),
    CLIENT_REFUND("거래처 환불"),
    CLIENT_EXCHANGE("거래처 교환");

    private final String name;

}
