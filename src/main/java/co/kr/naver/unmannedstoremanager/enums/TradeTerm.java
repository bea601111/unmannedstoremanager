package co.kr.naver.unmannedstoremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TradeTerm {

    FORWARD_PAY("선입금"),
    FOLLOW_MONTH_END("익월말"),
    FOLLOW_MONTH_10("익월10"),
    FOLLOW_MONTH_15("익월15"),
    FOLLOW_MONTH_25("익월25");

    private final String term;
}
