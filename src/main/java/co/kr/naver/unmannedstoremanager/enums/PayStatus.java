package co.kr.naver.unmannedstoremanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PayStatus {

    NORMAL("정상"),
    CANCEL("취소");

    private final String name;
}
