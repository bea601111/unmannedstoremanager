package co.kr.naver.unmannedstoremanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class ProductInputRequest {

    @NotNull
    @Min(0)
    @Max(10000)
    private Integer orderQty;

    @NotNull
    private LocalDate dateOrder;

}
