package co.kr.naver.unmannedstoremanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductInputDateQtyUpdateRequest {

    @NotNull
    @Min(0)
    @Max(10000)
    private Integer inputQty;
}
