package co.kr.naver.unmannedstoremanager.model;

import co.kr.naver.unmannedstoremanager.entity.ProductInput;
import co.kr.naver.unmannedstoremanager.enums.InputStatus;
import co.kr.naver.unmannedstoremanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductInputItem {
    private Long id;
    private Long productId;
    private Integer orderQty;
    private LocalDate dateOrder;
    private Integer inputQty;
    private LocalDate dateInput;
    private InputStatus inputStatus;
    private String memo;


    private ProductInputItem(Builder builder){
        this.id = builder.id;
        this.productId = builder.productId;
        this.orderQty = builder.orderQty;
        this.dateOrder = builder.dateOrder;
        this.inputQty = builder.inputQty;
        this.dateInput = builder.dateInput;
        this.inputStatus = builder.inputStatus;
        this.memo = builder.memo;
    }

    public static class Builder implements CommonModelBuilder<ProductInputItem> {
        private final Long id;
        private final Long productId;
        private final Integer orderQty;
        private final LocalDate dateOrder;
        private final Integer inputQty;
        private final LocalDate dateInput;
        private final InputStatus inputStatus;
        private final String memo;

        public Builder(ProductInput productInput){
            this.id = productInput.getId();
            this.productId = productInput.getProduct().getId();
            this.orderQty = productInput.getOrderQty();
            this.dateOrder = productInput.getDateOrder();
            this.inputQty = productInput.getInputQty();
            this.dateInput = productInput.getDateInput();
            this.inputStatus = productInput.getInputStatus();
            this.memo = productInput.getMemo();
        }

        @Override
        public ProductInputItem build() {
            return new ProductInputItem(this);
        }
    }
}