package co.kr.naver.unmannedstoremanager.model;

import co.kr.naver.unmannedstoremanager.entity.Contract;
import co.kr.naver.unmannedstoremanager.enums.ProductType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ProductRequest {

    private Contract contract;
    private String productName;
    private ProductType productType;
    private Double buyPrice;
    private Double sellPrice;
    private Integer stock;
    private String barCode;
    private LocalDate dateReg;
    private Double saveRate;
    private Double saleRate;
    private LocalDate dateSale;
    private String memo;
}
