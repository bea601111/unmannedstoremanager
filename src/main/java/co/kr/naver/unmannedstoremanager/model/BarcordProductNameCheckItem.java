package co.kr.naver.unmannedstoremanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class BarcordProductNameCheckItem {

    @NotNull
    @Length(min = 2, max = 100)
    private String productName;

    @NotNull
    @Min(0)
    @Max(1000000)
    private Double sellPrice;

    @NotNull
    @Min(0)
    @Max(1000)
    private Integer stock;

    @NotNull
    @Min(0)
    @Max(1000000)
    private Double saleRate;

    private LocalDate dateSale;

}
