package co.kr.naver.unmannedstoremanager.model;

import co.kr.naver.unmannedstoremanager.enums.ProductType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SoldOutDescItem {

    private String productName;
    private ProductType productType;
    private Double buyPrice;
    private Double sellPrice;
}
