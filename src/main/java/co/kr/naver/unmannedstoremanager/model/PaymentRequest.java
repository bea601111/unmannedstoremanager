package co.kr.naver.unmannedstoremanager.model;

import co.kr.naver.unmannedstoremanager.enums.PayStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PaymentRequest {

    @NotNull
    @Min(1)
    @Max(100)
    private Integer payQty;

    @NotNull
    @Min(0)
    @Max(1000000)
    private Integer usePoint;

    private String memo;

}
