package co.kr.naver.unmannedstoremanager.model;

import co.kr.naver.unmannedstoremanager.entity.Contract;
import co.kr.naver.unmannedstoremanager.enums.TradeTerm;
import co.kr.naver.unmannedstoremanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractItem {
    private Long id;
    private String contractCompany;
    private String businessNumber;
    private String address;
    private String contractName;
    private String contractPhoneNum;
    private String contractEmail;

    @Enumerated(EnumType.STRING)
    private TradeTerm tradeTerm;

    private ContractItem(Builder builder){
        this.id = builder.id;
        this.contractCompany = builder.contractCompany;
        this.businessNumber = builder.businessNumber;
        this.address = builder.address;
        this.contractName = builder.contractName;
        this.contractPhoneNum = builder.contractPhoneNum;
        this.contractEmail = builder.contractEmail;
        this.tradeTerm = builder.tradeTerm;

    }

    public static class Builder implements CommonModelBuilder<ContractItem> {
        private final Long id;
        private final String contractCompany;
        private final String businessNumber;
        private final String address;
        private final String contractName;
        private final String contractPhoneNum;
        private final String contractEmail;
        private final TradeTerm tradeTerm;


        public Builder(Contract contract){
            this.id = contract.getId();
            this.contractCompany = contract.getContractCompany();
            this.businessNumber = contract.getBusinessNumber();
            this.address = contract.getAddress();
            this.contractName = contract.getContractName();
            this.contractPhoneNum = contract.getContractPhoneNumber();
            this.contractEmail = contract.getContractEmail();
            this.tradeTerm = contract.getTradeTerm();
        }

        @Override
        public ContractItem build() {
            return new ContractItem(this);
        }
    }

}
