package co.kr.naver.unmannedstoremanager.model;

import co.kr.naver.unmannedstoremanager.enums.TradeTerm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ContractRequest {

    @NotNull
    @Length(min = 1,max = 50)
    private String contractCompany;

    @NotNull
    @Length(min = 10,max = 10)
    private String businessNumber;

    @NotNull
    @Length(min = 2,max = 100)
    private String address;

    @NotNull
    @Length(min = 1,max = 30)
    private String contractName;

    @NotNull
    @Length(min = 11,max = 13)
    private String contractPhoneNum;

    @NotNull
    @Length(min = 5,max = 30)
    private String contractEmail;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TradeTerm tradeTerm;

    private String memo;
}
