package co.kr.naver.unmannedstoremanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaticsLeftStockResponse {
    private Integer leftStock;
}
