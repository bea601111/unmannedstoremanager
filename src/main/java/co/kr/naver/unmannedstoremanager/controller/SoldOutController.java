package co.kr.naver.unmannedstoremanager.controller;

import co.kr.naver.unmannedstoremanager.model.SoldOutDescItem;
import co.kr.naver.unmannedstoremanager.service.SoldOutDescService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "매진 임박 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sold-out")
public class SoldOutController {
    private final SoldOutDescService soldOutDescService;

    @ApiOperation(value = "매진 임박 상품 리스트 보기")
    @GetMapping("/all")
    public List<SoldOutDescItem> getSoldOuts(){
        return soldOutDescService.getSoldOuts();
    }
}
