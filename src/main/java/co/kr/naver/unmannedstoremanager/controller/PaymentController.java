package co.kr.naver.unmannedstoremanager.controller;

import co.kr.naver.unmannedstoremanager.entity.Member;
import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.model.PaymentRequest;
import co.kr.naver.unmannedstoremanager.service.MemberService;
import co.kr.naver.unmannedstoremanager.service.PaymentService;
import co.kr.naver.unmannedstoremanager.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "결제 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/payment")
public class PaymentController {
    private final ProductService productService;
    private final MemberService memberService;
    private final PaymentService paymentService;

/*    @ApiOperation(value = "결제 등록")
    @PostMapping("/product-id{productId}/member-id/{memberId}")
    public String setPayment(@PathVariable long productId, @PathVariable long memberId, @RequestBody @Valid PaymentRequest paymentRequest){
        Product product = productService.getOriginData(productId);
        Member member = memberService.getMemberData(memberId);

        paymentService.setPayment(product,member,paymentRequest);



        return "ok";
    }*/
}
