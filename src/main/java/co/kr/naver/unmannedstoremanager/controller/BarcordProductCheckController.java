package co.kr.naver.unmannedstoremanager.controller;

import co.kr.naver.unmannedstoremanager.model.BarcordProductNameCheckItem;
import co.kr.naver.unmannedstoremanager.service.BarcordProductCheckService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "바코드 정보 조회")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/barcord")
public class BarcordProductCheckController {
    private final BarcordProductCheckService barcordProductCheckService;

    @ApiOperation(value = "바코드 번호로 정보 조회")
    @GetMapping("/data/barcord-num")
    public List<BarcordProductNameCheckItem> getBarcordProductCheck(@RequestParam("barcord") String barcordNum){
        return barcordProductCheckService.getBarcordProductCheck(barcordNum);
    }
}
