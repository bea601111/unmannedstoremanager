package co.kr.naver.unmannedstoremanager.controller;

import co.kr.naver.unmannedstoremanager.model.ContractItem;
import co.kr.naver.unmannedstoremanager.model.ContractRequest;
import co.kr.naver.unmannedstoremanager.service.ContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "거래처 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/client")
public class ContractController {
    private final ContractService contractService;

    @ApiOperation(value = "거래처를 등록")
    @PostMapping("/data")
    public String setContract(@RequestBody @Valid ContractRequest contractRequest){
        contractService.setContract(contractRequest);

        return "OK";
    }
    @ApiOperation(value = "거래처를 수정")
    @PutMapping("/data/{id}")
    public String putContract(@PathVariable long id, @RequestBody @Valid ContractRequest contractRequest){
        contractService.putContract(id, contractRequest);

        return "ok";
    }
    @ApiOperation(value = "거래처 리스트를 보여준다")
    @GetMapping("/all")
    public List<ContractItem> getContracts(){
        return contractService.getContracts();
    }
}
