package co.kr.naver.unmannedstoremanager.controller;

import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.model.ProductInputDateQtyUpdateRequest;
import co.kr.naver.unmannedstoremanager.model.ProductInputItem;
import co.kr.naver.unmannedstoremanager.model.ProductInputOrderQtyUpdateRequest;
import co.kr.naver.unmannedstoremanager.model.ProductInputRequest;
import co.kr.naver.unmannedstoremanager.service.ProductInputService;
import co.kr.naver.unmannedstoremanager.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "입고 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product-input")
public class ProductInputController {
    private final ProductInputService productInputService;
    private final ProductService productService;

    @ApiOperation(value = "입고 물품 등록하기")
    @PostMapping("/data/{productId}")
    public String setProductInput(@PathVariable long productId, @RequestBody @Valid ProductInputRequest productInputRequest){
        Product product = productService.getOriginData(productId);
        productInputService.setProductInput(product, productInputRequest);

        return "OK";
    }
    @ApiOperation(value = "발주 수량 및 메모 수정하기")
    @PutMapping("/order-qty-memo/{orderMemoId}")
    public String putProductInputOrderQty(@PathVariable long orderMemoId, @RequestBody @Valid ProductInputOrderQtyUpdateRequest productInputOrderQtyUpdateRequest){
        productInputService.putProductInputOrderQty(orderMemoId,productInputOrderQtyUpdateRequest);

        return "OK";
    }
    @ApiOperation(value = "입고 완료 체크하기 및 재고 수량에 추가하기")
    @PutMapping("/date-add-qty/{id}")
    public String putProductInputDateQty(@PathVariable long id, @RequestBody @Valid ProductInputDateQtyUpdateRequest productInputDateQtyUpdateRequest){
        productInputService.putProductInputDateQty(id,productInputDateQtyUpdateRequest);

        return "OK";
    }
    @ApiOperation(value = "입고 상태 취소로 수정하기")
    @PutMapping("/status-cancel/{id}")
    public String putProductInputDateQty(@PathVariable long id){
        productInputService.putProductInputStatusCancel(id);
        return "OK";
    }
    @ApiOperation(value = "입출고 리스트 보여주기")
    @GetMapping("/all")
    public List<ProductInputItem> getProductInputs(){
        return productInputService.getProductInputs();
    }
}
