package co.kr.naver.unmannedstoremanager.controller;

import co.kr.naver.unmannedstoremanager.model.BarcordProductNameCheckItem;
import co.kr.naver.unmannedstoremanager.service.ProductNameCheckService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "상품명 조회")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product-name")
public class ProductNameCheckController {
    private final ProductNameCheckService productNameCheckService;

    @ApiOperation(value = "상품명으로 정보 조회")
    @GetMapping("/data/search")
    public List<BarcordProductNameCheckItem> getProductNameCheck(@RequestParam("name") String productName){
        return productNameCheckService.getProductNameCheck(productName);
    }
}
