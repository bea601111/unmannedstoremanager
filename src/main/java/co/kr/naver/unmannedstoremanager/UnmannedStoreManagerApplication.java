package co.kr.naver.unmannedstoremanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnmannedStoreManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnmannedStoreManagerApplication.class, args);
	}

}
