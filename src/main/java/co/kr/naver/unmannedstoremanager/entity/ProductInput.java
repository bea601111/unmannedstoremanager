package co.kr.naver.unmannedstoremanager.entity;

import co.kr.naver.unmannedstoremanager.enums.InputStatus;
import co.kr.naver.unmannedstoremanager.interfaces.CommonModelBuilder;
import co.kr.naver.unmannedstoremanager.model.ProductInputDateQtyUpdateRequest;
import co.kr.naver.unmannedstoremanager.model.ProductInputOrderQtyUpdateRequest;
import co.kr.naver.unmannedstoremanager.model.ProductInputRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductInput {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품 id
    @ManyToOne
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    // 발주수량
    @Column(nullable = false)
    private Integer orderQty;

    // 발주일
    @Column(nullable = false)
    private LocalDate dateOrder;

    // 입고수량
    @Column(nullable = false)
    private Integer inputQty;

    // 입고일
    private LocalDate dateInput;

    // 입고상태
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private InputStatus inputStatus;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    public void cancelInputStatus(){
        this.inputStatus = InputStatus.CANCEL;
    }

    public void putProductInputDateQty(ProductInputDateQtyUpdateRequest productInputDateQtyUpdateRequest){
        this.inputQty = productInputDateQtyUpdateRequest.getInputQty();
        this.dateInput = LocalDate.now();
        this.inputStatus = InputStatus.COMPLETE;
    }

    public void putProductInputOrderQty(ProductInputOrderQtyUpdateRequest productInputOrderQtyUpdateRequest){
        this.orderQty = productInputOrderQtyUpdateRequest.getOrderQty();
        this.memo = productInputOrderQtyUpdateRequest.getMemo();
    }

    private ProductInput(Builder builder){
        this.product = builder.product;
        this.orderQty = builder.orderQty;
        this.dateOrder = builder.dateOrder;
        this.inputQty = builder.inputQty;
        this.dateInput = builder.dateInput;
        this.inputStatus = builder.inputStatus;
        this.memo = builder.memo;
    }

    public static class Builder implements CommonModelBuilder<ProductInput>{

        private final Product product;
        private final Integer orderQty;
        private final LocalDate dateOrder;
        private final Integer inputQty;
        private final LocalDate dateInput;
        private final InputStatus inputStatus;
        private final String memo;

        public Builder(Product product, ProductInputRequest productInputRequest){
            this.product = product;
            this.orderQty = productInputRequest.getOrderQty();
            this.dateOrder = productInputRequest.getDateOrder();
            this.inputQty = 0;
            this.dateInput = null;
            this.inputStatus = InputStatus.WAIT;
            this.memo = null;
        }


        @Override
        public ProductInput build() {
            return new ProductInput(this);
        }
    }
}