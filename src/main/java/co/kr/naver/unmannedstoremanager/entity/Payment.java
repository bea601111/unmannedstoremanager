package co.kr.naver.unmannedstoremanager.entity;

import co.kr.naver.unmannedstoremanager.enums.PayStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Payment {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품 id
    @ManyToOne
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    // 고개 id
    @ManyToOne
    @JoinColumn(name = "memberId")
    private Member member;

    // 결제상태
    @Column(nullable = false)
    private PayStatus payStatus;

    // 결제수량
    @Column(nullable = false)
    private Integer payQty;

    // 적립포인트
    @Column(nullable = false)
    private Integer savePoint;

    // 사용포인트
    @Column(nullable = false)
    private Integer usePoint;

    // 결제금액
    @Column(nullable = false)
    private Double payPrice;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

}





