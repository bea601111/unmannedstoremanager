package co.kr.naver.unmannedstoremanager.entity;

import co.kr.naver.unmannedstoremanager.enums.OutStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class ProductOutput {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 상품 id
    @ManyToOne
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    // 출고수량
    @Column(nullable = false)
    private Integer outputQty;

    // 출고일
    @Column(nullable = false)
    private LocalDate dateOutput;

    // 출고상태
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private OutStatus outputStatus;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;
}
