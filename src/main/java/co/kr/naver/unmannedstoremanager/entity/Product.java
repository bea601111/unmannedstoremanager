package co.kr.naver.unmannedstoremanager.entity;

import co.kr.naver.unmannedstoremanager.enums.ProductType;
import co.kr.naver.unmannedstoremanager.interfaces.CommonModelBuilder;
import co.kr.naver.unmannedstoremanager.model.ProductRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 거래처 id
    @ManyToOne
    @JoinColumn(name = "contractId")
    private Contract contract;

    // 상품명
    @Column(nullable = false, length = 100)
    private String productName;

    // 분류코드
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private ProductType productType;

    // 매입가
    @Column(nullable = false)
    private Double buyPrice;

    // 판매가
    @Column(nullable = false)
    private Double sellPrice;

    // 재고수량
    @Column(nullable = false)
    private Integer stock;

    // 바코드
    @Column(unique = true, nullable = false, length = 13)
    private String barCode;

    // 등록일
    @Column(nullable = false)
    private LocalDate dateReg;

    // 적립률
    @Column(nullable = false)
    private Double saveRate;

    // 할인금액
    @Column(nullable = false)
    private Double saleRate;

    // 할인기간
    private LocalDate dateSale;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    public void inputStock(int afterQty){
        this.stock += afterQty;
    }

    private Product(Builder builder){
        this.contract = builder.contract;
        this.productName = builder.productName;
        this.productType = builder.productType;
        this.buyPrice = builder.buyPrice;
        this.sellPrice = builder.sellPrice;
        this.stock = builder.stock;
        this.barCode = builder.barCode;
        this.dateReg = builder.dateReg;
        this.saveRate = builder.saveRate;
        this.saleRate = builder.saleRate;
        this.dateSale = builder.dateSale;
        this.memo = builder.memo;

    }
    public static class Builder implements CommonModelBuilder<Product>{

        private final Contract contract;
        private final String productName;
        private final ProductType productType;
        private final Double buyPrice;
        private final Double sellPrice;
        private final Integer stock;
        private final String barCode;
        private final LocalDate dateReg;
        private final Double saveRate;
        private final Double saleRate;
        private final LocalDate dateSale;
        private final String memo;

        public Builder(ProductRequest productRequest){

            this.contract = productRequest.getContract();
            this.productName = productRequest.getProductName();
            this.productType = productRequest.getProductType();
            this.buyPrice = productRequest.getBuyPrice();
            this.sellPrice = productRequest.getSellPrice();
            this.stock = productRequest.getStock();
            this.barCode = productRequest.getBarCode();
            this.dateReg = productRequest.getDateReg();
            this.saveRate = productRequest.getSaveRate();
            this.saleRate = productRequest.getSaleRate();
            this.dateSale = productRequest.getDateSale();
            this.memo = productRequest.getMemo();
        }
        @Override
        public Product build() {
            return new Product(this);
        }
    }
}
