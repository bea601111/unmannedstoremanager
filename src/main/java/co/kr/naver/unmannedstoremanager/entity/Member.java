package co.kr.naver.unmannedstoremanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Member {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    // 가입일
    @Column(nullable = false)
    private LocalDate dateJoin;

    // 고객명
    @Column(nullable = false, length = 30)
    private String memberName;

    // 연락처
    @Column(unique = true, nullable = false, length = 13)
    private String phoneNumber;

    // 포인트
    @Column(nullable = false)
    private Integer memberPoint;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;
}



