package co.kr.naver.unmannedstoremanager.entity;

import co.kr.naver.unmannedstoremanager.enums.TradeTerm;
import co.kr.naver.unmannedstoremanager.interfaces.CommonModelBuilder;
import co.kr.naver.unmannedstoremanager.model.ContractRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Contract {

    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 거래처
    @Column(nullable = false, length = 50)
    private String contractCompany;

    // 사업자등록번호
    @Column(unique = true, nullable = false, length = 12)
    private String businessNumber;

    // 주소
    @Column(nullable = false, length = 100)
    private String address;

    // 담당자
    @Column(nullable = false, length = 30)
    private String contractName;

    // 연락처
    @Column(nullable = false, length = 13)
    private String contractPhoneNumber;

    // 이메일
    @Column(length = 30)
    private String contractEmail;

    //거래조건
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private TradeTerm tradeTerm;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    public void putContract(ContractRequest contractRequest){
        this.contractCompany = contractRequest.getContractCompany();
        this.businessNumber = contractRequest.getBusinessNumber();
        this.address = contractRequest.getAddress();
        this.contractName = contractRequest.getContractName();
        this.contractPhoneNumber = contractRequest.getContractPhoneNum();
        this.contractEmail = contractRequest.getContractEmail();
        this.tradeTerm = contractRequest.getTradeTerm();
        this.memo = contractRequest.getMemo();
    }

    private Contract(Builder builder){
        this.contractCompany = builder.contractCompany;
        this.businessNumber = builder.businessNumber;
        this.address = builder.address;
        this.contractName = builder.contractName;
        this.contractPhoneNumber = builder.contractPhoneNumber;
        this.contractEmail = builder.contractEmail;
        this.tradeTerm = builder.tradeTerm;
        this.memo = builder.memo;
    }

    public static class Builder implements CommonModelBuilder<Contract> { //이건 하나의 자그마한 엔티티라고 볼 수도 있다.
        //빌더는 id를 제외한 똑같은 변수들을 가지며, 임시공간? 같은 느낌인 것 같다.
        private final String contractCompany;
        private final String businessNumber;
        private final String address;
        private final String contractName;
        private final String contractPhoneNumber;
        private final String contractEmail;
        private final TradeTerm tradeTerm;
        private final String memo;

        public Builder(ContractRequest contractRequest){ //빌더가 set을 대신한다고 생각하면 편할 거다.
            this.contractCompany = contractRequest.getContractCompany(); //여기서 this는 78줄부터
            this.businessNumber = contractRequest.getBusinessNumber();
            this.address = contractRequest.getAddress();
            this.contractName = contractRequest.getContractName();
            this.contractPhoneNumber = contractRequest.getContractPhoneNum();
            this.contractEmail = contractRequest.getContractEmail();
            this.tradeTerm = contractRequest.getTradeTerm();
            this.memo = null;

        }

        @Override
        public Contract build() {
            return new Contract(this);
        }
    }
}