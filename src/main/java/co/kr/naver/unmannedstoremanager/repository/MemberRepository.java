package co.kr.naver.unmannedstoremanager.repository;

import co.kr.naver.unmannedstoremanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member,Long> {
}
