package co.kr.naver.unmannedstoremanager.repository;

import co.kr.naver.unmannedstoremanager.entity.ProductOutput;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductOutputRepository extends JpaRepository<ProductOutput,Long> {
}
