package co.kr.naver.unmannedstoremanager.repository;

import co.kr.naver.unmannedstoremanager.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment,Long> {
}
