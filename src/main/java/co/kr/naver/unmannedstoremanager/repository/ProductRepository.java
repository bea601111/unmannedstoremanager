package co.kr.naver.unmannedstoremanager.repository;

import co.kr.naver.unmannedstoremanager.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Long> {
    List<Product> findAllByBarCode(String barCode);
    List<Product> findAllByProductName(String productName);

    List<Product> findAllByStockLessThanEqualOrderByStock(int counts);
}
