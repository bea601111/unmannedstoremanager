package co.kr.naver.unmannedstoremanager.repository;

import co.kr.naver.unmannedstoremanager.entity.ProductInput;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInputRepository extends JpaRepository<ProductInput,Long> {
}
