package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.model.SoldOutDescItem;
import co.kr.naver.unmannedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SoldOutDescService {
    private final ProductRepository productRepository;

    public List<SoldOutDescItem> getSoldOuts(){
        List<Product> originList = productRepository.findAllByStockLessThanEqualOrderByStock(10);

        List<SoldOutDescItem> result = new LinkedList<>();

        originList.forEach(e -> {
            SoldOutDescItem getData = new SoldOutDescItem();

            getData.setProductName(e.getProductName());
            getData.setProductType(e.getProductType());
            getData.setBuyPrice(e.getBuyPrice());
            getData.setSellPrice(e.getSellPrice());

            result.add(getData);
        });
        return result;
    }
}
