package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public Product getOriginData(long id){
        return productRepository.findById(id).orElseThrow();
    }
}
