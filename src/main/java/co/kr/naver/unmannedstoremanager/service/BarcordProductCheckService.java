package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.model.BarcordProductNameCheckItem;
import co.kr.naver.unmannedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BarcordProductCheckService {
    private final ProductRepository productRepository;

    public List<BarcordProductNameCheckItem> getBarcordProductCheck(String barCode){ // 원본 데이터 전체를 넘겨 줄 순 없으니 보여줄 정보만 따로 그릇에 담아 보여줄 예정이다.
        //그릇의 이름은 바코드의 번호를 입력했을 때와 이름을 입력해서 검색했을 때 보여주는 항목은 같기 때문에 같은 그릇을 사용했다
        //이 서비스는 '바코드 Barcord' 를 '검색 Check' 하여 '물품 Product'을 '보여주는 get' 기능이기 때문에 위와 같이 명명했다.
        List<Product> originList = productRepository.findAllByBarCode(barCode);
        //바코드의 이름을 입력 받아 그 값과 일치하는 물품을 보여줄 예정이기 때문에 ProductRepository에서 findAllByBarCord를 새로 인지시켜 주었다.
        //왜냐하면 Repository의 기본 기능에는 들어가있지 않는 기능이기 때문이다.
        //입력한 'barCord' 값을 검색해서 모두 찾은 후 'findAllBy' Product 모양 틀에 받아올 것인데, 하나만 받아오는 것이 아니기 때문에 List를 사용한다.
        //이것의 이름은 originList로 명명한다

        List<BarcordProductNameCheckItem> result = new LinkedList<>();
        //return으로 넘겨줘야 하는 타입이 List<BarcordProductNameCheckItem>이기 때문에 아래에 나오는 값들을 하나씩 엮어서 리스트로 만들어준다(LinkedList)

        originList.forEach(e -> { // for문으로 아래의 문장을 반복할 것인데, originList 안에 있는 값(e)를 하나씩 다음에 (->{}) 넘겨주겠다
            BarcordProductNameCheckItem getData = new BarcordProductNameCheckItem();
            //그릇 안에 정해진 정보만을 넘겨줄 예정이므로 그릇의 틀을(BarcordProductNameCheckItem) 새로 만들어(new) getData라고 명명한 뒤, 값을 담을 준비를 한다.

            getData.setProductName(e.getProductName());
            getData.setSellPrice(e.getSellPrice());
            getData.setStock(e.getStock());
            getData.setSaleRate(e.getSaleRate());
            getData.setDateSale(e.getDateSale());

            //값을 다 담았으면 값을 엮어 리스트로 만드는 result에 추가(add)하여 return 시킨다.
            result.add(getData);
        });
        return result;
    }
}
