package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Contract;
import co.kr.naver.unmannedstoremanager.model.ContractItem;
import co.kr.naver.unmannedstoremanager.model.ContractRequest;
import co.kr.naver.unmannedstoremanager.repository.ContractRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractService {
    private final ContractRepository contractRepository; // Contract의 창고지기와 협업한다

    public void setContract(ContractRequest contractRequest){ //고객(Contract)를 등록(set) 시키는 기능이기 때문에 다음과 같이 명명했으며
        //고객에게 등록시킬 정보와 저장 할 정보를 그릇으로 만들어 입력 받게 만들었다

        Contract addData = new Contract.Builder(contractRequest).build(); //Contract에 안에 Builder가 미리 지어놨기 때문에 가져오기만 하면 된다.
        // 정보를 담을 곳은 고객 정보를 담을 창고(Contract)이기 때문에 Contract 틀의 새로 만들고(new) 정보(Data)를 추가(add)할 것이기 때문에 다음과 같이 명명한다.

//        addData.setContractCompany(contractRequest.getContractCompany());
//        addData.setBusinessNumber(contractRequest.getBusinessNumber());
//        addData.setAddress(contractRequest.getAddress());
//        addData.setContractName(contractRequest.getContractName());
//        addData.setContractPhoneNumber(contractRequest.getContractPhoneNum());
//        addData.setContractEmail(contractRequest.getContractEmail());
//        addData.setTradeTerm(contractRequest.getTradeTerm());


        contractRepository.save(addData);
    }
    public void putContract(long id, ContractRequest contractRequest){ //고객(Contract)의 정보를 수정(put)시키는 기능이기 때문에 다음과 같이 명명했으며
        //수정 시킬 값도 등록 시켰을 때 받았던 정보와 다르지 않으므로 같은 그릇을 넣는다. 하지만 id를 추가로 입력 받아 해당 고객의 정보만 수정할 수 있게 한다.

        Contract putData = contractRepository.findById(id).orElseThrow();
        //해당 고객의 정보만 수정하기 위해선 해당 id를 가져와야 하기 때문에 창고지기(contractRepository)에게 해당 id를 찾아오도록(findById)지시하고, 만약 id가 없다면 취소하도록(orElseThrow)한다.

          putData.putContract(contractRequest); //putData는 Contract라는 틀임에는 변함이 없기에 Contract 안에 있는 빌더를 이용해 미리 지어놓은 putContract를 가져와 사용한다

//        putData.setContractCompany(contractRequest.getContractCompany());
//        putData.setBusinessNumber(contractRequest.getBusinessNumber());
//        putData.setAddress(contractRequest.getAddress());
//        putData.setContractName(contractRequest.getContractName());
//        putData.setContractPhoneNumber(contractRequest.getContractPhoneNum());
//        putData.setContractEmail(contractRequest.getContractEmail());
//        putData.setTradeTerm(contractRequest.getTradeTerm());

        //위의 문장들을 실행한 후, putData라는 이름의 틀에 데이터가 담겨있는 상태이므로, 이 틀을 창고지기에게 저장(save)시키도록 지시한다
        contractRepository.save(putData);
    }
    public List<ContractItem> getContracts(){ //고객(Contract)의 정보를 보여주는(get) 기능이므로 다음과 같이 명명하였고
        //데이터 하나만 가져올 것이 아니므로 List를 사용한다. 모든 정보를 다 보여줄 게 아니니 그릇에 보여줄 데이터의 틀만 넣어준다.

        List<Contract> originData = contractRepository.findAll();
        //모든 데이터를 불러올 것이므로 해당 창고지기에게 모두 찾아오도록(findAll) 지시한다. 그 데이터는 Contract의 데이터이니
        //Contract모양의 리스트의 originData라는 이름의 틀에 넣는다.

        List<ContractItem> result = new LinkedList<>();
        //return으로 넘겨줘야 하는 타입이 List<ContractItem>이기 때문에 아래에 나오는 값들을 하나씩 엮어서 리스트로 만들어준다(LinkedList)

        originData.forEach(e -> { // for문으로 아래의 문장을 반복할 것인데, originList 안에 있는 값(e)를 하나씩 다음에 (->{}) 넘겨주겠다
            ContractItem getData = new ContractItem.Builder(e).build(); //Contract에 안에 Builder가 미리 지어놨기 때문에 가져오기만 하면 된다.
            //그릇 안에 정해진 정보만을 넘겨줄 예정이므로 그릇의 틀을(ContractItem) 새로 만들어(new) getData라고 명명한 뒤, 값을 담을 준비를 한다.

            /*getData.setId(e.getId());
            getData.setContractCompany(e.getContractCompany());
            getData.setBusinessNumber(e.getBusinessNumber());
            getData.setAddress(e.getAddress());
            getData.setContractName(e.getContractName());
            getData.setContractPhoneNum(e.getContractPhoneNumber());
            getData.setContractEmail(e.getContractEmail());
            getData.setTradeTerm(e.getTradeTerm());*/

            //위의 문장들을 실행한 후, getData라는 이름의 틀에 데이터가 담겨있는 상태이므로, 이를 result가 엮어 리스트로 만들어 return 시킨다.
            result.add(getData);
        });
        return result;
    }
}
