package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Member;
import co.kr.naver.unmannedstoremanager.entity.Payment;
import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.enums.PayStatus;
import co.kr.naver.unmannedstoremanager.model.PaymentRequest;
import co.kr.naver.unmannedstoremanager.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentService {
    private final PaymentRepository paymentRepository;
    public void setPayment(Product product, Member member, PaymentRequest paymentRequest){
        Payment setData = new Payment();

        setData.setProduct(product);
        setData.setMember(member);
        setData.setPayStatus(PayStatus.NORMAL);
        setData.setPayQty(paymentRequest.getPayQty());

        setData.setUsePoint(paymentRequest.getUsePoint());

        double payPrice = product.getSellPrice() * paymentRequest.getPayQty() - setData.getUsePoint();
        setData.setPayPrice(payPrice);

        int savePoint = (int) (payPrice * product.getSaveRate());
        setData.setSavePoint(savePoint);

        setData.setMemo(paymentRequest.getMemo());

        int beforeQty = setData.getProduct().getStock();
        int minusQty = setData.getPayQty();
        int afterQty = beforeQty - minusQty;

        setData.getProduct().setStock(afterQty);

        paymentRepository.save(setData);



    }
}
