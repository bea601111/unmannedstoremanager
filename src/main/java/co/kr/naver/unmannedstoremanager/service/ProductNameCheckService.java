package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.model.BarcordProductNameCheckItem;
import co.kr.naver.unmannedstoremanager.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductNameCheckService {
    private final ProductRepository productRepository;

    public List<BarcordProductNameCheckItem> getProductNameCheck(String productName){
        List<Product> originList = productRepository.findAllByProductName(productName);

        List<BarcordProductNameCheckItem> result = new LinkedList<>();

        originList.forEach(e -> {
            BarcordProductNameCheckItem getData = new BarcordProductNameCheckItem();

            getData.setProductName(e.getProductName());
            getData.setSellPrice(e.getSellPrice());
            getData.setStock(e.getStock());
            getData.setSaleRate(e.getSaleRate());
            getData.setDateSale(e.getDateSale());

            result.add(getData);
        });
        return result;
    }
}
