package co.kr.naver.unmannedstoremanager.service;

import co.kr.naver.unmannedstoremanager.entity.Product;
import co.kr.naver.unmannedstoremanager.entity.ProductInput;
import co.kr.naver.unmannedstoremanager.enums.InputStatus;
import co.kr.naver.unmannedstoremanager.model.ProductInputDateQtyUpdateRequest;
import co.kr.naver.unmannedstoremanager.model.ProductInputItem;
import co.kr.naver.unmannedstoremanager.model.ProductInputOrderQtyUpdateRequest;
import co.kr.naver.unmannedstoremanager.model.ProductInputRequest;
import co.kr.naver.unmannedstoremanager.repository.ProductInputRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductInputService {
    private final ProductInputRepository productInputRepository; //ProductInput 창고지기랑 협업한다
    public void setProductInput(Product product, ProductInputRequest productInputRequest){ // 입고(ProductInput)를 등록(set)하는 기능이므로 다음과 같이 명명하였고
        //입력 받게 할 정보는 ProductRequest에 정의하고, ProductInputService는 Product라는 걸 모르니, 나중에 Product 같은 게 있다~ 같이 인지시켜놓는다

        ProductInput addData = new ProductInput.Builder(product,productInputRequest).build(); //ProductInput 안의 Builder가 미리 지어놨기 때문에 가져오기만 하면 된다.
        //창고에 담아 저장할 예정이므로 ProductInput이라는 틀을 새로 만들고(new) addData라고 명명한다.

        /*addData.setProduct(product); //setProduct에는 아직 모르는데 이런 게 있다고 인지시켜 놓는다
        addData.setOrderQty(productInputRequest.getOrderQty());
        addData.setDateOrder(productInputRequest.getDateOrder());
        addData.setInputQty(0); //setInputQty에는 초기 값으로 0을 설정해 놓는다
        addData.setInputStatus(InputStatus.WAIT); //setInputStatus에는 enum 값인 WAIT를 사용한다*/

        //위의 문장들을 실행한 후, addData라는 이름의 틀에 데이터가 담겨있는 상태이므로, 이 틀을 창고지기에게 저장(save)시키도록 지시한다
        productInputRepository.save(addData);
    }
    public void putProductInputOrderQty(long id, ProductInputOrderQtyUpdateRequest productInputOrderQtyUpdateRequest){
        //입고에서 (ProductInput) 발주량을 (OrderQty) 수정할 것이므로 (put) 다음과 같이 명명하고, id를 받아 그릇에 담겨진 값을 수정할 것이므로 id와 Request를 하나씩 받는다)
        ProductInput putData = productInputRepository.findById(id).orElseThrow();

        putData.putProductInputOrderQty(productInputOrderQtyUpdateRequest); //putData는 ProductInput이라는 틀임에는 변함이 없기에 ProductInput 안에 있는 빌더를 이용해
        // 미리 지어놓은 putProductInputOrderQty 가져와 사용할 수 있다

        /*putData.setOrderQty(productInputOrderQtyUpdateRequest.getOrderQty());
        putData.setMemo(productInputOrderQtyUpdateRequest.getMemo());*/

        //위를 실행하고 putData에 수정한 정보가 담겨있으므로, 창고지기에게 putData를 저장하도록(save) 지시한다
        productInputRepository.save(putData);
    }
    public void putProductInputDateQty(long id, ProductInputDateQtyUpdateRequest productInputDateQtyUpdateRequest){ //대부분은 위와 동일하다. 입고가 완료되면 입고된 날짜와 수량을 등록하고
        //입고된 수 만큼 재고에 등록하는 기능이다.
        ProductInput putData = productInputRepository.findById(id).orElseThrow();

        putData.putProductInputDateQty(productInputDateQtyUpdateRequest);//putData는 ProductInput이라는 틀임에는 변함이 없기에 ProductInput 안에 있는 빌더를 이용해
        // 미리 지어놓은 putProductInputDateQty 가져와 사용할 수 있다

        /*putData.setInputQty(productInputDateQtyUpdateRequest.getInputQty());
        putData.setDateInput(LocalDate.now()); // 입고된 날짜는 수정할 때의 시간이 들어가도록 설정한다.
        putData.setInputStatus(InputStatus.COMPLETE); //상태는 enum의 COMPLETE가 입력된다.*/

        int beforeQty = putData.getProduct().getStock(); //입고 수량이 추가되기 전
        int plusQty = putData.getInputQty(); //입고된 수량
        int afterQty = beforeQty + plusQty; //입고 수량이 추가된 재고 수량

        putData.getProduct().inputStock(afterQty); //위와 마찬가지로 미리 지어놓은 것을 가져와 사용할 수 있다.
//        putData.getProduct().setStock(afterQty); //입고 수량이 추가된 재고 수량을 FK로 연결되어 있는 Product 안에 Stock 항목에 넣는다

        //이를 창고지기에게 저장하도록 지시한다.
        productInputRepository.save(putData);
    }
    public void putProductInputStatusCancel(long id){ //위와 거의 동일하다. 상태를 취소로 변경하는 기능이다.
        ProductInput putData = productInputRepository.findById(id).orElseThrow();

        putData.cancelInputStatus(); //위와 동일
//        putData.setInputStatus(InputStatus.CANCEL); //상태를 enum의 CANCEL로 변경하고

        //이를 창고지기에게 저장시킨다.
        productInputRepository.save(putData);
    }
/*    public List<ProductInputItem> getProductInputs(){ //위와 거의 동일하다. 리스트의 전체를 보여주는 것이다.
        List<ProductInput> originList = productInputRepository.findAll();

        List<ProductInputItem> result = new LinkedList<>();

        originList.forEach(e ->{
            ProductInputItem getData = new ProductInputItem();

            getData.setId(e.getId());
            getData.setProductId(e.getProduct().getId());
            getData.setOrderQty(e.getOrderQty());
            getData.setDateOrder(e.getDateOrder());
            getData.setInputQty(e.getInputQty());
            getData.setDateInput(e.getDateInput());
            getData.setInputStatus(e.getInputStatus());
            getData.setMemo(e.getMemo());

            result.add(getData);
        });
        return result;
    }*/
    public List<ProductInputItem> getProductInputs() { //위와 거의 동일하다. 리스트의 전체를 보여주는 것이다.
        List<ProductInput> originList = productInputRepository.findAll();

        List<ProductInputItem> result = new LinkedList<>();

        originList.forEach(e -> {
            result.add(new ProductInputItem.Builder(e).build()); //result에 들어갈 값은 한 줄 밖에 없기 때문에 add 안에 직접 넣을 수 있다.

/*            getData.setId(e.getId());
            getData.setProductId(e.getProduct().getId());
            getData.setOrderQty(e.getOrderQty());
            getData.setDateOrder(e.getDateOrder());
            getData.setInputQty(e.getInputQty());
            getData.setDateInput(e.getDateInput());
            getData.setInputStatus(e.getInputStatus());
            getData.setMemo(e.getMemo());*/

        });
        return result;
    }

}
