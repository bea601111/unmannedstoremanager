package co.kr.naver.unmannedstoremanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}